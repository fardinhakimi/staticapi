
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path')
const fs = require('fs')
const axios = require('axios')
const app = express()

// APP CONFIGURATION AND MIDDLEWARE

// CORS 
app.use(cors())
// ENABLE PRE-FLIGHT SUPPORT FOR ALL ROUTES
app.options('*', cors())
// BODY PARSER 
app.use(bodyParser.json())
// ACCESS ALLOW ORIGIN
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

// API ROUTES ARE BELOW

app.get('/', function(req, res, next){
    res.send({"heartbeat": "server is up and running"});
});

function paginate( businesses, perPage, page )
{
    return businesses.slice(perPage*(page-1), perPage*page);
}
function compareValues(key,sortBy,order) {
    return function(a, b) {
      if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
          return 0; 
      }
      const varA = (typeof a[key][sortBy] === 'string') ? a[key][sortBy].toUpperCase() : a[key][sortBy];
      const varB = (typeof b[key][sortBy] === 'string') ? b[key][sortBy].toUpperCase() : b[key][sortBy];
      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order == 'dsc') ? (comparison * -1) : comparison
      );
    };
  }

// COLLECTION AND FILTERING 
app.get('/collection', function(req, res, next){
    let businesses = []
    const filterQuery= req.query
    const supportedFilters = {
        category: '',
        municipality:'',
        place: '',
        page: '',
        search:'',
        sortBy:'',
        sortType:''
    }
    const supportedSorts = ['name','website','phone','updated_at','created_at']
    if (filterQuery) {
        for (filterItem in filterQuery){
            if(supportedFilters.hasOwnProperty(filterItem)) {
                supportedFilters[filterItem] = filterQuery[filterItem]
        }
    }
   }
    // GET ALL BUSINESSES
    businesses = JSON.parse(fs.readFileSync('data/all_business.json', 'utf8'))
    // FILTER BY CATEGORY
    if(supportedFilters.category){
        businesses = businesses.filter(function(business){
            if(business.category.name == supportedFilters.category) {
                return business
            }
        })
    }
    // FILTER BY MUNICIPALITY
    if(supportedFilters.municipality){
        businesses = businesses.filter(function(business){
            if(business.address_full.municipality.name == supportedFilters.municipality) {
                return business
            }
        })
    }
    // FILTER BY PLACE 
    if(supportedFilters.place){
        businesses = businesses.filter(function(business){
            if(business.address_full.place.name == supportedFilters.place) {
                return business
            }
        })
    }
    // VERY SIMPLE SEARCH FOR TESTING ( FOR TESTING PURPOSE WE ONLY SUPPORT SEARCH BY NAME)
    if(supportedFilters.search){
        const search = supportedFilters.search.toLowerCase()
        businesses = businesses.filter(function(item){
            const businessName = item.business.name.toLowerCase()
            if(businessName.includes(search) || businessName == search) {
                return businesses
            }
        })
    }
    // PAGINATION PART
    const total = businesses.length
    const perPage = 5
    if (supportedFilters.page) {
        const page = supportedFilters.page
        businesses = paginate( businesses, perPage, page)
    }

    // SORT AND ORDER
    if(supportedFilters.sortBy){
        // FOR TESTING WE ONLY SUPPORT NAME, WEBSITE, PHONE NUMBER, CREATED_AT 
        // AND UPDATED_AT
        const sortBy = supportedFilters.sortBy
        const sortType =  supportedFilters.sortType

        if(supportedSorts.includes(sortBy)){
            businesses.sort(compareValues('business',sortBy,sortType))
        }
    }

    res.send({'all':businesses, 'total':total})
});

// CATEGORIES
app.get('/categories', function(req, res, next){
    const categories = JSON.parse(fs.readFileSync('data/categories.json', 'utf8'))
    res.send(categories)
});
// MUNICIPALITIES
app.get('/municipalities', function(req, res, next){
    const municipalities = JSON.parse(fs.readFileSync('data/munis.json','utf8'))
    res.send(municipalities).status(200)
});
// PLACES FOR EACH MUNICIPALITY
app.get('/places/:id', function(req, res, next){
    const id = req.params.id
    let places = []

    // GISLAVED PLACES
    if(id == 1) {
        places = JSON.parse(fs.readFileSync('data/places_gislaved.json','utf8'))
    }
    // KUNGSBACKA PLACES
    if(id == 2) {
        places = JSON.parse(fs.readFileSync('data/places_kungsbacka.json','utf8'))
    }
    // JONKOPING PLACES 
    if(id == 3) {
        places = JSON.parse(fs.readFileSync('data/places_jonkoping.json','utf8'))
    }

    res.send(places).status(200)
})
app.put('/business',function(req, res, next){
    // create a new business
    const content = req.body
    res.send(content)
})
app.patch('/business/:id',function(req, res, next){
    // patch a new business
    const business = req.body
    res.send(business)
})
app.delete('/business/:id',function(req, res, next){
    res.send({"detail": "success"}).status(200)
})

app.listen(3000, function(err){
    if(!err)
    console.log('app listening on port '+ 3000);
});


/*

BUSINESS DETAILS 

business_address {
    street_name:
    street_number:
    zipcode:
}
owner_name,
business_name,
description (optional(100)),

*/
